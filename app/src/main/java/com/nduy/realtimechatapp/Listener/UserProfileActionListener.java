package com.nduy.realtimechatapp.Listener;

import android.view.View;

public interface UserProfileActionListener {
    void onClick(View view, int position, boolean isLongClick);
}
