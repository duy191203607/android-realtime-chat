package com.nduy.realtimechatapp.Activity;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nduy.realtimechatapp.Model.User;
import com.nduy.realtimechatapp.Utils.DBCollectionConstant;
import com.nduy.realtimechatapp.Utils.PreferenceManager;
import com.nduy.realtimechatapp.databinding.CameraThumnailImagePreviewBinding;
import com.shashank.sony.fancytoastlib.FancyToast;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

@RequiresApi(api = Build.VERSION_CODES.N)
public class CameraActivity extends BaseActivity {
    private CameraThumnailImagePreviewBinding binding;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private Uri mImageUri;
    PreferenceManager preferenceManager;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private DocumentReference documentReference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = CameraThumnailImagePreviewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        preferenceManager = new PreferenceManager(getApplicationContext());
        dispatchTakePictureIntent();
        eventInit();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                mImageUri = FileProvider.getUriForFile(this,
                        "com.nduy.realtimechatapp.provider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Toast.makeText(this, "Image saved", Toast.LENGTH_SHORT).show();
            try {
                Matrix matrix = new Matrix();
                matrix.postRotate(90);
                Bitmap dstBmp;
                Bitmap srcBmp = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageUri);
                if (srcBmp.getWidth() >= srcBmp.getHeight()) {
                    dstBmp = Bitmap.createBitmap(
                            srcBmp,
                            srcBmp.getWidth() / 2 - srcBmp.getHeight() / 2,
                            0,
                            srcBmp.getHeight(),
                            srcBmp.getHeight()
                    );

                } else {
                    dstBmp = Bitmap.createBitmap(
                            srcBmp,
                            0,
                            srcBmp.getHeight() / 2 - srcBmp.getWidth() / 2,
                            srcBmp.getWidth(),
                            srcBmp.getWidth()
                    );
                }
                Bitmap rotatedBitmap = Bitmap.createBitmap(dstBmp, 0, 0, dstBmp.getWidth(), dstBmp.getHeight(), matrix, true);
                binding.cameraThumbnailPreviewImage.setImageBitmap(rotatedBitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void eventInit() {
        binding.previewCameraTryAgain.setOnClickListener(v -> {
            dispatchTakePictureIntent();
        });
        binding.previewCameraOk.setOnClickListener(v -> {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();
            StorageReference ref = storageReference.child("images/" + UUID.randomUUID().toString());
            ref.putFile(mImageUri)
                    .addOnSuccessListener(taskSnapshot -> {
                        progressDialog.dismiss();
                        ref.getDownloadUrl().addOnSuccessListener(uri -> {
                            System.out.println("## Stored path is " + uri.toString());
                            updateUserAvatar(uri.toString());
                            preferenceManager.putString(User.IMAGE_FIELD, uri.toString());
                            startActivity(new Intent(this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            finish();
                        });
                    })
                    .addOnFailureListener(e -> {
                        progressDialog.dismiss();
                        FancyToast.makeText(getApplicationContext(), e.getMessage(), FancyToast.LENGTH_SHORT, FancyToast.ERROR, false).show();
                    });
        });
    }

    private void updateUserAvatar(String newUrlProfile) {
        FirebaseFirestore database = FirebaseFirestore.getInstance();
        documentReference = database.collection(DBCollectionConstant.User).document(preferenceManager.getString(User.User_ID));
        documentReference.update(User.IMAGE_FIELD, newUrlProfile);
    }
}

