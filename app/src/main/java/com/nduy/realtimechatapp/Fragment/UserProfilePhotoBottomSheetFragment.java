package com.nduy.realtimechatapp.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;

import com.nduy.realtimechatapp.Activity.CameraActivity;
import com.nduy.realtimechatapp.databinding.UserProfileBottomSheetBinding;

@RequiresApi(api = Build.VERSION_CODES.N)
@SuppressLint("RestrictedApi")

public class UserProfilePhotoBottomSheetFragment extends DialogFragment {
    public UserProfileBottomSheetBinding binding;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = UserProfileBottomSheetBinding.inflate(inflater, container, false);
        init();
        return binding.getRoot();
    }
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        init();
//    }

//    @Override
//    public void setupDialog(@NonNull Dialog dialog, int style) {
//        super.setupDialog(dialog, style);
//        //Set the custom view
//        @SuppressLint("InflateParams") View view = LayoutInflater.from(getContext()).inflate(R.layout.user_profile_bottom_sheet, null);
//        dialog.setContentView(view);
//        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
//        CoordinatorLayout.Behavior behavior = params.getBehavior();
//
//        if (behavior instanceof BottomSheetBehavior) {
//            ((BottomSheetBehavior) behavior).setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//                @Override
//                public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                    String state = "";
//
//                    switch (newState) {
//                        case BottomSheetBehavior.STATE_DRAGGING: {
//                            state = "DRAGGING";
//                            break;
//                        }
//                        case BottomSheetBehavior.STATE_SETTLING: {
//                            state = "SETTLING";
//                            break;
//                        }
//                        case BottomSheetBehavior.STATE_EXPANDED: {
//                            state = "EXPANDED";
//                            break;
//                        }
//                        case BottomSheetBehavior.STATE_COLLAPSED: {
//                            state = "COLLAPSED";
//                            break;
//                        }
//                        case BottomSheetBehavior.STATE_HIDDEN: {
//                            dismiss();
//                            state = "HIDDEN";
//                            break;
//                        }
//                    }
//                }
//
//                @Override
//                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//                }
//            });
//        }
//    }

    private void init() {
        binding.takeAPhoto.setOnClickListener(view -> {
            startCamera();
        });
    }

    private void startCamera() {
        Intent intent = new Intent(getActivity(), CameraActivity.class);
        startActivity(intent);
    }
}
