package com.nduy.realtimechatapp.Adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.nduy.realtimechatapp.Activity.LoginActivity;
import com.nduy.realtimechatapp.Activity.MainActivity;
import com.nduy.realtimechatapp.Fragment.UserProfilePhotoBottomSheetFragment;
import com.nduy.realtimechatapp.Listener.UserProfileActionListener;
import com.nduy.realtimechatapp.Model.User;
import com.nduy.realtimechatapp.Model.UserAction;
import com.nduy.realtimechatapp.R;
import com.nduy.realtimechatapp.Utils.DBCollectionConstant;
import com.nduy.realtimechatapp.Utils.PreferenceManager;
import com.shashank.sony.fancytoastlib.FancyToast;

import java.util.HashMap;
import java.util.List;

public class UserActionAdapter extends RecyclerView.Adapter<UserActionAdapter.UserActionViewHolder> {
    private final List<UserAction> listUserAction;
    private final Context context;

    private UserProfilePhotoBottomSheetFragment userProfilePhotoBottomSheetFragment;

    public UserActionAdapter(List<UserAction> listUserAction, Context context) {
        this.listUserAction = listUserAction;
        this.context = context;
    }

    @NonNull
    @Override
    public UserActionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView = layoutInflater.inflate(R.layout.container_profile_detail_item, parent, false);
        return new UserActionViewHolder(itemView);
    }

    @SuppressLint("ResourceType")
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(@NonNull UserActionViewHolder holder, int position) {
        holder.actionImage.setImageResource(listUserAction.get(position).getIconID());
        holder.actionName.setText(listUserAction.get(position).getActionName());
        holder.setItemClickListener((view, position1, isLongClick) -> {
            switch (position1) {
                case 0: {
                    userProfilePhotoBottomSheetFragment = new UserProfilePhotoBottomSheetFragment();
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    userProfilePhotoBottomSheetFragment.show(activity.getSupportFragmentManager(), "TAG");
                    break;
                }
                case 1: {
                    FirebaseFirestore database = FirebaseFirestore.getInstance();
                    PreferenceManager preferenceManager = new PreferenceManager(context);
                    String userID = preferenceManager.getString(User.User_ID);
                    if (userID != null) {
                        DocumentReference documentReference =
                                database.collection(DBCollectionConstant.User).document(
                                        userID
                                );
                        HashMap<String, Object> updateUser = new HashMap<>();
                        updateUser.put(User.USER_FCM_TOKEN, FieldValue.delete());
                        documentReference.update(updateUser)
                                .addOnSuccessListener(unused -> {
                                    preferenceManager.clear();
                                    context.startActivity(new Intent(context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                })
                                .addOnFailureListener(e -> {
                                    FancyToast.makeText(context, e.getMessage(), FancyToast.LENGTH_SHORT, FancyToast.INFO, false);
                                });
                    }
                }
                default: {
                    System.out.print("cc");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listUserAction.size();
    }

    private void showBottomSheet(Context context, int resource) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(resource, null);
        final Dialog mBottomSheetDialog = new Dialog(context);
        mBottomSheetDialog.setContentView(v);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
    }

    static class UserActionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private UserProfileActionListener userProfileActionListener;
        public TextView actionName;
        public ImageView actionImage;

        public UserActionViewHolder(View view) {
            super(view);
            actionImage = view.findViewById(R.id.profileDetailItemImage);
            actionName = view.findViewById(R.id.profileDetailItemName);
            view.setOnClickListener(this);
        }

        public void setItemClickListener(UserProfileActionListener userProfileActionListener) {
            this.userProfileActionListener = userProfileActionListener;
        }

        @Override
        public void onClick(View view) {
            userProfileActionListener.onClick(view, getAdapterPosition(), false);
        }
    }
}
