FROM ubuntu:18.04

RUN apt-get update && \
      apt-get -y install sudo

RUN useradd -m docker && echo "docker:docker" | chpasswd && adduser docker sudo

USER docker
RUN sudo apt-get update
RUN sudo apt-get install build-essential
RUN sudo apt install ruby ruby-dev
RUN export LC_ALL=en_US.UTF-8
RUN export LANG=en_US.UTF-8
RUN source ~/.profile
RUN gem install google-api-client
RUN gem install fastlane -NV